<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dikap','DikapController@index');

Route::post('/dikap/create','DikapController@create');
Route::get('/dikap/{id}/edit','DikapController@edit');
Route::post('/dikap/{id}/update','DikapController@update');
Route::get('/dikap/{id}/delete','DikapController@delete');