<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DikapController extends Controller
{
    public function index()
    {
    	$data_dikap = \App\Dikap::all();
    	return view('dikap.index',['data_dikap' =>$data_dikap]);
    }
    public function create(Request $request)
    {
    	\App\Dikap::create($request->all());
    	return redirect('/dikap');
    }
    public function edit($id)
    {
    	$dikap = \App\Dikap::find($id);
    	return view('dikap/edit',['dikap' =>$dikap]);
    }
     public function update(Request $request,$id)
    {
    	$dikap = \App\Dikap::find($id);
    	$dikap->update($request->all());
    	return redirect('/dikap');
    }
    public function delete($id)
    {
    	$dikap = \App\Dikap::find($id);
    	$dikap->delete($dikap);
    	return redirect('/dikap');
    }
}
