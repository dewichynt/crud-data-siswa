<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dikap extends Model
{
    protected $table = 'dikap';
    protected $fillable = ['nama_lengkap','jenis_kelamin','agama','alamat'];
}
