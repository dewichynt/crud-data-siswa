	@extends('layouts.master')

@section('content')
		<h1>Edit</h1>
		<div class="row">
			<div class="col-lg-12">
			       <form action="/dikap/{{$dikap->id}},/update" method="POST">
					        	{{csrf_field()}}
						  <div class="form-group">
						    <label for="exampleInputEmail1">Nama Lengkap</label>
						    <input name="nama_lengkap" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap" value="{{$dikap->nama_lengkap}}">
						    </div>

						    <div class="form-group">
						    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
						    <select class="form-control" id="exampleFormControlSelect1" name="jenis_kelamin">
						      <option value="L" @if($dikap->jenis_kelamin == 'L') selected @endif>Laki-Laki</option>
						      <option value="P" @if($dikap->jenis_kelamin == 'P') selected @endif>Perempuan</option>
						       </select>
							 </div>

							 <div class="form-group">
						    <label for="exampleInputEmail1">Agama</label>
						    <input  name="agama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Agama" value="{{$dikap->agama}}">
						    </div>

						  <div class="form-group">
						    <label for="exampleInputPassword1">Alamat</label>
						     <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alamat">{{$dikap->alamat}}</textarea>
						  </div>	
						 <button type="submit" class="btn btn-secondary btn-sm">Update</button>
						</form>
					</div>
					</div>
				</div>
				

	@endsection
