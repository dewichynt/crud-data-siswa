@extends('layouts.master')

@section('content')
		<div class="row">
			<div class="col-6">
				<h1>Shawdikap</h1>	
			</div>

			<div class="col-6">				
			<!-- Button trigger modal -->
<button type="button" class="btn btn-outline-secondary float-right" data-toggle="modal" data-target="#exampleModal">
  Tambah
</button>



			</div>
					<table class="table table-bordered">
						<thead class="thead-dark">
						<tr>
							<th>Nama Lengkap</th>
							<th>Jenis Kelamin</th>
							<th>Agama</th>
							<th>Alamat</th>
							<th>Aksi</th>
						</tr>
						</thead>
						@foreach($data_dikap as $dikap)
						<tr>	
							<td>{{$dikap->nama_lengkap}}</td>
							<td>{{$dikap->jenis_kelamin}}</td>
							<td>{{$dikap->agama}}</td>
							<td>{{$dikap->alamat}}</td>
							<td><a href="/dikap/{{$dikap->id}}/edit" class="btn-outline-secondary btn-sm">Edit</a>
								<a href="/dikap/{{$dikap->id}}/delete" class="btn-outline-secondary btn-sm" onclick="return confirm('Yakin mau dihapus?')">Delete</a>
							</td>
						</tr>
						@endforeach
					</thead>
					</table>
		</div>
	</div>

	<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<!--formulir-->
       <form action="/dikap/create" method="POST">
					        	{{csrf_field()}}
						  <div class="form-group">
						    <label for="exampleInputEmail1">Nama Lengkap</label>
						    <input name="nama_lengkap" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Lengkap">
						    </div>

						    <div class="form-group">
						    <label for="exampleFormControlSelect1">Jenis Kelamin</label>
						    <select class="form-control" id="exampleFormControlSelect1" name="jenis_kelamin">
						      <option value="L">Laki-Laki</option>
						      <option value="P">Perempuan</option>
						       </select>
							 </div>

							 <div class="form-group">
						    <label for="exampleInputEmail1">Agama</label>
						    <input  name="agama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Agama">
						    </div>

						  <div class="form-group">
						    <label for="exampleInputPassword1">Alamat</label>
						     <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="alamat"></textarea>
						  </div>	  
											      
						</div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					        <button type="submit" class="btn btn-primary">Submit</button>
						</form>
      </div>
    </div>
  </div>
</div>
	
@endsection